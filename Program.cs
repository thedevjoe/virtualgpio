﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace VirtualGpio
{
    [AttributeUsage(AttributeTargets.Method)]
    public class GpioEntryPoint : Attribute
    {
        public GpioEntryPoint()
        {
        }
    }
    class Program
    {
        internal static UdpClient PinUdp = null;
        internal static Config Conf = null;
        internal static GPIO GPIO = new GPIO();
        static void Main(string[] args)
        {
            Conf = JsonConvert.DeserializeObject<Config>(File.ReadAllText("conf.json"));
            foreach (byte b in Conf.pins)
            {
                GPIO.Switches.Add(b, new GPIO.Switch()
                {
                    Direction = GPIO.Direction.Unused,
                    On = false
                });
            }
            Console.WriteLine("Initializing Udp Client.");
            PinUdp = new UdpClient(Conf.port);
            _ = UdpThread();
            Console.WriteLine("Beginning Assembly Load");
            if (!Directory.Exists("modules")) Directory.CreateDirectory("modules");
            foreach (string name in Directory.EnumerateFiles("modules"))
            {
                Assembly a = Assembly.LoadFile(Path.GetFullPath(name));
                MethodInfo entry = null;
                foreach (Type t in a.GetTypes())
                {
                    foreach (MethodInfo i in t.GetMethods())
                    {
                        var x = i.GetCustomAttribute(typeof(PdioEntryPoint));
                        if (x != null) entry = i;
                    }
                }

                if (entry == null)
                {
                    Console.WriteLine("Warning: Invalid module " + a);
                    continue;
                }
                entry.Invoke(null, new object[] {GPIO});
                Console.WriteLine("Loaded assembly " + a + " successfully.");
            } 
            Console.WriteLine("VirtualPDIO is now running.");
            Task.Delay(3000).GetAwaiter().GetResult();
            _ = Display();
            Task.Delay(-1).GetAwaiter().GetResult();
        }

        private static async Task Display()
        {
            Console.Clear();
            bool bottomDrawn = false;
            while (true)
            {
                StringBuilder builder = new StringBuilder();
                Console.Write("|");
                builder.Append("|");
                for(int i=1;i<31;i++)
                {
                    bool pinExists = GPIO.Switches.ContainsKey((byte) i);
                    bool on = pinExists && GPIO.Switches[(byte) i].On;
                    if (!pinExists)
                    {
                        Console.BackgroundColor = ConsoleColor.Gray;
                        Console.Write(" X");
                        Console.ResetColor();
                        Console.Write("|");
                    } else if (on)
                    {
                        Console.BackgroundColor = ConsoleColor.Green;
                        Console.Write(" H");
                        Console.ResetColor();
                        Console.Write("|");
                    }
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.Write(" L");
                        Console.ResetColor();
                        Console.Write("|");
                    }

                    if (i < 10) builder.Append("0");
                    builder.Append(i + "|");
                }

                if (!bottomDrawn)
                {
                    Console.WriteLine();
                    Console.Write(builder.ToString());
                    bottomDrawn = true;
                }
                
                Console.CursorLeft = 0;
                Console.CursorTop = 4;
                foreach (string s in GPIO.AdditionalInfo.Keys)
                {
                    Console.Write(s + ": " + GPIO.AdditionalInfo[s].info + "\t\t\t\t\t\n");
                }
                
                Console.CursorTop = 0;
                await Task.Delay(10);
            }
        }

        private static async Task UdpThread()
        {
            while (true)
            {
                var res = await PinUdp.ReceiveAsync();
                if (res.Buffer.Length != 2) continue;
                byte port = res.Buffer[0];
                bool on = res.Buffer[1] == (byte) 1;
                GPIO.Switch s;
                if (!GPIO.Switches.TryGetValue(port, out s)) continue;
                if (s.Direction != GPIO.Direction.Rx)
                {
                    Debug.WriteLine("Got INVALID update from " + res.RemoteEndPoint + " - Port " + port + " now " + (on ? "on" : "off"));
                }

                s.On = on;
                //Debug.WriteLine("Got update from " + res.RemoteEndPoint + " - Port " + port + " now " + (on ? "on" : "off"));
            }
        }

        public static GPIO GetGPIO()
        {
            return GPIO;
        }
    }
}