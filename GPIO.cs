﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json.Linq;

namespace VirtualGpio
{
    public class GPIO
    {
        internal GPIO()
        {
            
        }
        
        internal class Switch
        {
            internal bool On;
            internal Direction Direction = Direction.Unused;
        }

        public enum Direction
        {
            Tx,
            Rx,
            Unused
        }
        internal static Dictionary<byte, Switch> Switches = new Dictionary<byte, Switch>();
        public Dictionary<string, AdditionalInfo> AdditionalInfo = new Dictionary<string, AdditionalInfo>();

        public bool Get(byte port)
        {
            if (Switches[port].Direction != Direction.Rx) return false;
            return Switches[port].On;
        }

        public void Set(byte port, bool on)
        {
            if (Switches[port].Direction != Direction.Tx) return;
            if (Switches[port].On == on) return;
            Switches[port].On = on;
            
            // Forward if possible
            bool has = Program.Conf.connections.ContainsKey(port + "");
            if (!has) return;
            Config.Connection obj = Program.Conf.connections[port + ""].ToObject<Config.Connection>();
            byte[] send = new byte[] {obj.port, on ? (byte)1 : (byte)0};
            Program.PinUdp.Send(send, 2, obj.host, obj.pport);
            //Debug.WriteLine("Sent pin update to " + obj.host + " - Port " + port + " is now " + (on ? "on" : "off"));
        }

        public void SetDirection(byte port, Direction direction)
        {
            Switches[port].Direction = direction;
        }
    }
}