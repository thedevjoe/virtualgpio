﻿using Newtonsoft.Json.Linq;

namespace VirtualGpio
{
    public class Config
    {
        public byte[] pins;
        public JObject connections;
        public int port;

        public class Connection
        {
            public string host;
            public int pport;
            public byte port;
        }
    }
}